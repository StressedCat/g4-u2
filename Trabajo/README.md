# G4-U2

Requisitos:
C++
graphviz


Para iniciar el programa:
1 Ingrese en el CMD el comando make para instalar el programa
  -> .../g4-u2/Trabajo$ make
2 Se creará un programa con nombre de Cargo, para ingresar a este
haga lo siguiente:
  -> .../g4-u2/Trabajo$ ./programa

En el programa:
  El programa consiste en simular un almacenamiento de arbol binario, en donde
  se podrá graficar el arbol que se realiza, realizar inserciones, ver en
  diferentes ordenes, eliminar un valor y modificar un valor.

Para borrar el programa:
Asegurese que esté en la misma carpeta donde corrio el programa
-> .../g4-u2/Trabajo
Ingrese el comando make clean para borrar el programa que uso
-> .../g4-u2/Trabajo$ make clean
