#include <fstream>
#include <iostream>
#include <cstdio>
using namespace std;
#include "main.h"

class Grafo {
    private:

    public:
        Grafo (Nodo *nodo) {
            ofstream fp;

            /* se borran los archivos para actualizarlos */
            remove("grafo.txt");
            remove("grafo.png");

            /* se abre el archivo de texto */
            fp.open ("grafo.txt");

            /* Se coloca los parametros para el grafico */
            fp << "digraph G {" << endl;
            fp << "node [style=filled fillcolor=yellow];" << endl;

            /* se va a la función para recorrer los datos y llenar el texto */
            recorrer(nodo, fp);

            fp << "}" << endl;

            /* se cierra el archivo */
            fp.close();

            /* Se genera grafo con el archivo con los parametros ingresados */
            system("dot -Tpng -ografo.png grafo.txt &");

            /* se visualiza el grafo */
            system("eog grafo.png &");
        }

        /*
        * recorre en árbol en preorden y agrega datos al archivo.
        * los numeros se harán string para evitar errores al hacer el
        * grafico
        */
        void recorrer(Nodo *p, ofstream &fp) {
            string cadena = "\0";

            if (p != NULL) {
                if (p->izq != NULL) {
                    fp <<  p->info << "->" << p->izq->info << ";" << endl;
                } else {
                    cadena = "num" + to_string(p->info) + "i";
                    fp <<  cadena << " [shape=point];" << endl;
                    fp << p->info << "->" << cadena << ";" << endl;
                }

                if (p->der != NULL) {
                    fp << p->info << "->" << p->der->info << ";" << endl;
                } else {
                    cadena = "num" + to_string(p->info) + "d";
                    fp <<  cadena << " [shape=point];" << endl;
                    fp << p->info << "->" << cadena << ";" << endl;
                }

                recorrer(p->izq, fp);
                recorrer(p->der, fp);
            }
        }
};

void modnode(Nodo *raiz, int val, int change) {
  /* al llegar al valor que define el usuario, se cambiará a change */
  if (raiz->info == val){
    raiz->info = change;
  }

  else{
    /* si es menor al valor actual, se concentra en la izquierda */
    if(val < raiz->info){
      /* Sí no es null, se mueve a la izquierda */
      if (raiz->izq != NULL) {
        modnode(raiz->izq, val, change);
      }
      /* Sí es null, se termina la rama del arbol y no hay valor */
      else{
        cout << "El valor ingresado no fue encontrado" << endl;
      }
    }

    /* si es mayor al valor actual, se concentra en la derecha */
    else if(val > raiz->info){
      /* Sí no es null, se mueve a la derecha */
      if (raiz->der != NULL) {
        modnode(raiz->der, val, change);
      }
      /* Sí es null, se termina la rama del arbol y no hay valor */
      else{
        cout << "El valor ingresado no fue encontrado" << endl;
      }
    }
  }
}

int addnode(Nodo *raiz, int val) {
  /* si es menor al valor actual, se concentra en la izquierda */
  if(val < raiz->info){
    /* Sí no es null, se mueve a la izquierda */
    if (raiz->izq != NULL) {
      addnode(raiz->izq, val);
    }
    /* Sí es null, se crea el valor en la izquierda */
    else{
      raiz->izq = crearNodo(val);
    }
  }

  /* si es mayor al valor actual, se concentra en la derecha */
  else if(val > raiz->info){
    /* Se mueve a la derecha */
    if (raiz->der != NULL) {
      addnode(raiz->der, val);
    }
    /* Se crea en la derecha */
    else{
      raiz->der = crearNodo(val);
    }
  }

  /* Si no es mayor, ni es menor, entonces debe ser el numero */
  else{
    cout << "El numero ya se encuentra en el arbol" << endl;
  }
}

void delnode(Nodo *raiz, int val, Nodo *temp, bool dir) {
  /* al llegar al numero deseado, se transformará en NULL */
  if (raiz->info == val){
    if (dir == false) {
      if (raiz->izq != NULL) {
        dir = true;
        delnode(raiz, val, temp->izq, dir);
      }
      else if(raiz->der != NULL){
        dir = true;
        delnode(raiz, val, temp->der, dir);
      }
      else{
        raiz->info = NULL;
      }
    }
    else{
      raiz->info = temp->info;
      temp->info = NULL;
    }
  }

  else{
    /* si es menor al valor actual, va a la izquierda */
    if(val < raiz->info){
      if(raiz->izq != NULL){
        delnode(raiz->izq, val, raiz->izq, dir);
      }
      else{
        cout << "El numero no se encuentra en el arbol" << endl;
      }
    }

    /* si es mayor al valor actual, va a la derecha */
    else if(val > raiz->info){
      if(raiz->der != NULL){
        delnode(raiz->der, val, raiz->der, dir);
      }
      else{
        cout << "El numero no se encuentra en el arbol" << endl;
      }
    }
  }
}

void inorden(Nodo *raiz) {
   if (raiz != NULL){
     inorden(raiz->izq);
     cout << "[" << (raiz->info) << "] ";
     inorden(raiz->der);
   }
}

void preorden(Nodo *raiz){
  if(raiz != NULL){
    cout << "[" << (raiz->info) << "] ";
    preorden(raiz->izq);
    preorden(raiz->der);
  }
}

void posorden(Nodo *raiz){
  if(raiz != NULL){
    posorden(raiz->izq);
    posorden(raiz->der);
    cout << "[" << (raiz->info) << "] ";
  }
}

int main(void) {
    bool flick = true;
    int opt;
    Nodo *raiz = NULL;
    bool ident;
    int val;
    int change;

    /* el arbol va a tener numeros predeterminados */
    raiz = crearNodo(40);
    raiz->izq = crearNodo(20);
    raiz->der = crearNodo(60);

    raiz->izq->izq = crearNodo(10);
    raiz->izq->der = crearNodo(30);

    raiz->der->izq = crearNodo(50);
    raiz->der->der = crearNodo(70);

    /* Menu donde se seleccionará la acción */
    while(flick != false){
      cout << "=============================================" << endl;
      cout << "[1] Ingresar nuevo numero" << endl;
      cout << "[2] Eliminar un numero" << endl;
      cout << "[3] Modificar un numero" << endl;
      cout << "[4] Mostrar numeros" << endl;
      cout << "[5] Mostrar grafico de arbol" << endl;
      cout << "[0] Para terminar el programa" << endl;
      cout << "=============================================" << endl;
      cout << "Ingrese su opción: " << endl;
      cin >> opt;

      /* Añadir un valor al arbol */
      if (opt == 1) {
        cout << "Ingrese el valor que desea añadir" << endl;
        cin >> val;
        addnode(raiz, val);
      }

      /* Eliminar un valor al arbol */
      else if (opt == 2){
        cout << "Ingrese el valor que desea eliminar" << endl;
        cin >> val;
        delnode(raiz, val, raiz, false);
      }

      /* Modificar un valor existente en el arbol */
      else if (opt == 3){
        cout << "Ingrese el valor del numero que desea cambiar" << endl;
        cin >> val;
        cout << "Ingrese a cual numero desea cambiarlo" << endl;
        cin >> change;
        modnode(raiz, val, change);
      }

      /* ver los numeros en el arbol con diferentes metodos */
      else if (opt == 4){
        while (flick != false) {
          cout << "=============================================" << endl;
          cout << "Como desea ver los numeros del arbol" << endl;
          cout << "[1] En inorden" << endl;
          cout << "[2] En posorden" << endl;
          cout << "[3] En preorden" << endl;
          cout << "[4] Para verlo en las 3 formas" << endl;
          cout << "[0] Para volver al menu principal" << endl;
          cout << "=============================================" << endl;
          cout << "Ingrese su opción: " << endl;
          cin >> opt;
          if (opt == 1) {
            inorden(raiz);
            flick = false;
          }
          else if (opt == 2){
            posorden(raiz);
            flick = false;
          }
          else if (opt == 3){
            preorden(raiz);
            flick = false;
          }
          else if (opt == 4){
            cout << "En inorden: ";
            inorden(raiz);
            cout << endl << "En posorden: ";
            posorden(raiz);
            cout << endl << "En preorden: ";
            preorden(raiz);
            cout << endl;
            flick = false;
          }
          else if (opt == 0){
            flick = false;
          }
          else{
            cout << "Ingreso incorrecto, intente de nuevo" << endl;
          }
        }
        cout << endl;
        flick = true;
      }

      /* Graficar el arbol con graphviz */
      else if (opt == 5){
        Grafo *g = new Grafo(raiz);
      }

      /* terminar el programa */
      else if (opt == 0){
        flick = false;
      }

      /* Ingreso de valor inexistente */
      else{
        cout << "Su ingreso es incorrecto, intente de nuevo" << endl;
      }
    }
    return 0;
}
