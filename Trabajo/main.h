#include <fstream>
#include <iostream>
using namespace std;

/* estructura del nodo */
typedef struct _Nodo {
    int info;
    struct _Nodo *izq;
    struct _Nodo *der;
} Nodo;

/* se crea un nuevo Nodo */
Nodo *crearNodo(int dato) {
    Nodo *q;

    q = new Nodo();
    q->izq = NULL;
    q->der = NULL;
    q->info = dato;
    return q;
}
